# Sustainability

Forgejo depends on resources to keep going in a durable way and the most precious is the time people spend working on it. Most volunteers may not be inclined to account for the number of hours they spent working on organizing a videoconference, discussing a particular governance issue or implementing a feature. But those who are provide a valuable insight regarding the durability of Forgejo: when cumulated with the time spent by paid staff working alongside them, it is good approximation of what it takes for Forgejo to be sustainable.

For instance when one person is paid to spend four weeks working on a feature alongside a volunteer, that's at least 300 hours of work total, probably more if they are passionate and work long hours. In 2022 the salary of the person who is paid will vary between 5,000€ and 10,000€ depending on the country they live in and their level of expertise. And while volunteers are, by definition, not paid for their contribution it does not mean their time is less valuable.

All other costs running Forgejo are comparatively much lower, just like any other Free Software project. The hosting costs rely on donations to Codeberg e.V. which keep Codeberg.org running. Donations to Forgejo could cover everything else: hardware, travel costs etc.

# Forgejo resources per year

This is a manually maintained record of the resources dedicated to Forgejo updated at least once a year.

## 2023

### Grants

* 50,000€ [to work on the release process and the codebase](https://codeberg.org/forgejo/sustainability/issues/1#issuecomment-855819) for the benefit of Codeberg e.V., https://codeberg.org/oliverpool, https://codeberg.org/crystal paid by https://nlnet.nl/

### Hardware

* 3,150€ (total of 350€ per month from April to December included) KVM virtual machine 16 threads 64GB RAM 1TB SSD 1 IP sponsored by https://www.octopuce.fr

### Employee delegation

* 28,000€ (total of 100 days at the rate of 280€ per day) https://codeberg.org/dachary/ paid by https://easter-eggs.com

## 2022

### Donations

* 380€ donated to Codeberg and earmarked for Forgejo.

### Grants

* 50,000€ [to further forge federation](https://forum.forgefriends.org/t/nlnet-grant-application-for-federation-in-gitea-deadline-august-1st-2022/823) for the benefit of https://codeberg.org/xy, https://codeberg.org/6543, https://codeberg.org/realaravinth, https://codeberg.org/gusted, https://codeberg.org/dachary paid by https://nlnet.nl/

### Volunteer

* 4 hours https://codeberg.org/earl-warren https://codeberg.org/forgejo/forgejo/issues/57

### Employee delegation

* 12,600€ (25 days between October and December included at the rate of 280€ per day) https://codeberg.org/dachary/ paid by https://easter-eggs.com

### Donations

* 7.98 € forgejo.org domain name & mail services registration paid for by https://codeberg.org/oliverpool
* 9.25 € forgejo.com domain name registration paid for by https://codeberg.org/caesar
* 100€ static web hosting https://codeberg.org/forgejo/website/issues/25 paid for by https://codeberg.org/dachary

### In kind

* https://codeberg.org/forgejo hosting and CI by Codeberg e.V.
